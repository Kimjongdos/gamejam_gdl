﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JugadorMoviemintoVelocity : MonoBehaviour
{
    public int speed;
    public Rigidbody2D rb2d;

    public float TiempoFrenada = 0.25f;

    // Update is called once per frame
    void Update()
    {
        Movement();
    }

    public void Movement(){
        if(Input.GetKeyDown(KeyCode.DownArrow)){
            StartCoroutine(Down());
           
        }

    }

    IEnumerator Down(){
       
        
        //Instantiate(Particula,Arma.transform.position,Quaternion.identity); //Instancia Particula
         rb2d.velocity = new Vector2(0,speed);

       
        yield return new WaitForSeconds(TiempoFrenada);
        rb2d.AddForce(Vector2.right * speed);
        

    }
}
