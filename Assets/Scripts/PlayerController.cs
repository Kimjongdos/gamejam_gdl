﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private Vector2 direction;
    public float x;
    public float y;
    public float force;
    private float timeMoving = 0.2f;

    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        direction = Vector2.zero;
    }

    private void Update()
    {
        x = Input.GetAxisRaw("Horizontal");
        y = Input.GetAxisRaw("Vertical");
        direction = new Vector2(x, y);
        Movement();
        
    }

    void Movement(){
        StartCoroutine(Movimiento());
    }

    IEnumerator Movimiento(){
        rb2d.AddForce(- direction * force, ForceMode2D.Impulse);
        yield return new WaitForSeconds(timeMoving);
        rb2d.AddForce(direction * force, ForceMode2D.Impulse);
    }
}

    
