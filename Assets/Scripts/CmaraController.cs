﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CmaraController : MonoBehaviour
{
    private Transform target;

    private Vector3 position;

    void Awake()
    {
        target = GameObject.Find("Player").GetComponent<Transform>();
        position = new Vector3(target.transform.position.x, target.transform.position.y, -10f);
        transform.position = position;
    }

    private void Update()
    {
        position = new Vector3(target.transform.position.x, target.transform.position.y, -10f);
        transform.position = position;
    }
}
