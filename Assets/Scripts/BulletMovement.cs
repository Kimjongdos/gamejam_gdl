﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    public float speed;
    private PlayerMovement player;
    private Rigidbody2D rb2d;
    private GameObject sprite;

    private void Awake()
    {
        sprite = transform.gameObject.transform.GetChild(0).gameObject;
        player = GameObject.Find("Player").GetComponent<PlayerMovement>();
        rb2d = GetComponent<Rigidbody2D>();
        if (player.arriba){
            sprite.transform.rotation = Quaternion.Euler( 0, 0, -90);
            rb2d.velocity = (Vector2.up * speed * Time.deltaTime);
        }
        if(player.abajo){
            rb2d.velocity = (Vector2.down * speed * Time.deltaTime);
            sprite.transform.rotation = Quaternion.Euler( 0, 0, 90);
        }
        if(player.izquierda){
            rb2d.velocity = (Vector2.left * speed * Time.deltaTime);
            sprite.transform.rotation = Quaternion.Euler( 0, 0, 0);
        }
        if(player.derecha){
            rb2d.velocity =(Vector2.right * speed * Time.deltaTime);
            sprite.transform.rotation = Quaternion.Euler( 0, 0, 180);
        }
    }

    void Update(){
        Destroy(gameObject, 10f);
    }

    void OnTriggerEnter2D(Collider2D other){
        if(other.tag == "Limites"){
            Destroy(gameObject);
        }
    }


}
