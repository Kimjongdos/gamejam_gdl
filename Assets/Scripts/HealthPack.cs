﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPack : MonoBehaviour
{
    private Vector3 health;
    private Transform player;
    private ParticleSystem ps;
    private SpriteRenderer spriteR;

    void Awake(){
        player = GameObject.Find("Player").GetComponent<Transform>();
        health = new Vector3(0.2f, 0.2f, 0f);
        ps = GetComponent<ParticleSystem>();
        spriteR = GetComponent<SpriteRenderer>();
    }

    void OnTriggerEnter2D(Collider2D other){
        if(other.tag == "Player" && player.localScale.x < 1){
            player.localScale += health;
            ps.Play();
            spriteR.sprite = null;
            Destroy(gameObject, 1f);
        }
    }
}
