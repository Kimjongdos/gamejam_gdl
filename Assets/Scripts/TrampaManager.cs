﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrampaManager : MonoBehaviour
{

    public GameObject lanza;

    public Transform[] LanzaDown;   

    public float TimeToSpawn;
    private float counter;

    void Awake(){
        counter =  TimeToSpawn;
    }
    
    void Update(){
        counter += Time.deltaTime;
            if(counter >= TimeToSpawn){
                Lanzar();
                counter = 0;
            }
    }
    


    public void Lanzar(){
    Instantiate(lanza,LanzaDown[0].position,Quaternion.Euler(0,0,-180));
    Instantiate(lanza,LanzaDown[1].position,Quaternion.Euler(0,0,-180));
    Instantiate(lanza,LanzaDown[2].position,Quaternion.Euler(0,0,-180));
    Instantiate(lanza,LanzaDown[3].position,Quaternion.Euler(0,0,-180));
    }

}
