﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
   
   public AudioSource Disparo;
   public AudioSource Muerte;
    public GameObject FXDead;
    public int Fuerza;
    private Rigidbody2D rb2d;

    public float TiempoFrenada;
    private float currentTime;

    private Transform Arma;

//Sprites
    private SpriteRenderer sRenderer;
    public Sprite[] slimeSprite;

    private Quaternion rotacionL;
    private Quaternion rotacionR;
    private Quaternion rotacionU;
    private Quaternion rotacionD;

    private bool noControls;



    //Ataque
    public GameObject bala;
    public bool arriba;
    public bool abajo;
    public bool izquierda;
    public bool derecha;

    public GameObject psContainer;

    //Vida segun Mapa
    [Header("Vida")]
    private Vector3 initialScale;
    private Vector3 reduction;

    private float lifes1;
    private float lifes2;
    private float lifes3;
    private float lifes4;
    private float lifesColiseo;

    private float tiempoMuerto = 1.5f;
    private float currentMuerto;



    //Animations
    private Animator anim;

    //Angulo
    private Vector2 distance;
    private float angle;

    public float fuerzaEmpuje;   
    public float tiempoDeslizandose; 
    

    void Awake(){
        rb2d = GetComponent<Rigidbody2D>();
        Arma = this.gameObject.transform.GetChild(0);
        currentTime = 0f;
        psContainer.transform.position = Arma.transform.position;
        noControls = false;
        anim = GetComponent<Animator>();
        sRenderer = GetComponent<SpriteRenderer>();
        sRenderer.sprite = slimeSprite[0];
        currentMuerto = 0;
         //Vidas
        initialScale = new Vector3( 1f, 1f, 1f);

        if(SceneManager.GetActiveScene().name == "GamePlayScreen"){
            lifes1 = 16;
            reduction = transform.localScale/lifes1;   
        }
         else if(SceneManager.GetActiveScene().name == "Level2"){
            lifes2 = 50; // Son 46 tiros
            reduction = transform.localScale/lifes2;
        }
        else if(SceneManager.GetActiveScene().name == "Level3"){
            lifes3 = 100;
            reduction = transform.localScale/lifes3;
        }
        else if(SceneManager.GetActiveScene().name == "Level4"){
            lifes4 = 120;
            reduction = transform.localScale/lifes4;
        }
        else if(SceneManager.GetActiveScene().name == "Coliseo"){
            lifesColiseo = 150;
            reduction = transform.localScale/lifesColiseo;
        }


    }

    void Update()
    {
        if(!noControls){
            Movement();
        }
        else if(noControls){
            currentMuerto += Time.deltaTime;
            //Debug.Log(currentMuerto);
        } 

        currentTime += Time.deltaTime;
        psContainer.transform.position = Arma.transform.position;
        if(transform.localScale.x <= 0.1  ){
            Die();
        }


        //No nos hacemos mas grandes que 1
        if(transform.localScale.x > 1f){
            transform.localScale = initialScale;
        }        

    }

    public void Movement(){
        if(currentTime >= TiempoFrenada){  // Cadencia 
            //Te vas para la Derecha  
            if(Input.GetKeyDown(KeyCode.LeftArrow)){
                Disparo.Play();
                StartCoroutine(Left());
                Reduction();
                currentTime = 0;
                psContainer.transform.rotation = Quaternion.Euler( -90f, 0f, 90f);
                    
            }
            //""Izquierda
            if(Input.GetKeyDown(KeyCode.RightArrow)){
                Disparo.Play();
                StartCoroutine(Right());
                Reduction();
                currentTime = 0;
                psContainer.transform.rotation = Quaternion.Euler(-90f, 0f, -90f);

            }
            //""Abajo
            if(Input.GetKeyDown(KeyCode.DownArrow)){
                Disparo.Play();
                StartCoroutine(Up());
                Reduction();
                currentTime = 0;
                psContainer.transform.rotation = Quaternion.Euler( 180f, 0f, 0f);

            }
            //""Arriba
            if(Input.GetKeyDown(KeyCode.UpArrow)){
                Disparo.Play();
                StartCoroutine(Down());
                Reduction();
                currentTime = 0;
                psContainer.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
            }
        }

    }


    //METODOS DE VIDA Y MUERTE

        private void Reduction(){
            transform.localScale -= reduction;
            lifes1--;
            Instantiate(psContainer,Arma.position,transform.rotation); 
        }

    IEnumerator Left(){
        izquierda = true;
            
        //Animacion
        sRenderer.sprite = slimeSprite[1];
        rb2d.velocity = (Vector2.right * Fuerza);

       // Instantiate(Particula,Arma.transform.position,Quaternion.identity);
        rotacionL = Quaternion.Euler(0,0,-90); 
        transform.rotation = rotacionL;
            
        Instantiate(bala, Arma.transform.position, Quaternion.Euler(0,0,-90));


        yield return new WaitForSeconds(TiempoFrenada);
        rb2d.velocity = Vector2.zero;
        izquierda = false;
        sRenderer.sprite = slimeSprite[0];

    }

    IEnumerator Right(){
        derecha = true;
        sRenderer.sprite = slimeSprite[1];
            
        rb2d.velocity = (-Vector2.right * Fuerza);
        //Instantiate(Particula,Arma.transform.position,Quaternion.identity); //Instancia Particula
        Instantiate(bala,Arma.transform.position,transform.rotation);


        rotacionR = Quaternion.Euler(0,0,90);
        transform.rotation =  rotacionR;
        yield return new WaitForSeconds(TiempoFrenada);
        rb2d.velocity = Vector2.zero;
        derecha = false;
        sRenderer.sprite = slimeSprite[0];

    }

     IEnumerator Up(){
         abajo = true;
         sRenderer.sprite = slimeSprite[1];

        rb2d.velocity = (Vector2.up * Fuerza);
       // Instantiate(Particula,Arma.transform.position,Quaternion.identity);
        
        Instantiate(bala,Arma.transform.position,transform.rotation);


        transform.rotation = Quaternion.Euler(0,0,0);  
        yield return new WaitForSeconds(TiempoFrenada);
        rb2d.velocity = Vector2.zero;
        abajo = false;
        sRenderer.sprite = slimeSprite[0];
    }

    IEnumerator Down(){
        arriba = true;
        sRenderer.sprite = slimeSprite[1];

        rb2d.velocity = (-Vector2.up * Fuerza);
        //Instantiate(Particula,Arma.transform.position,transform.rotation);
        transform.rotation = Quaternion.Euler(0,0,180);  

        Instantiate(bala,Arma.transform.position,transform.rotation);

        yield return new WaitForSeconds(TiempoFrenada);
        rb2d.velocity = Vector2.zero;
        arriba = false;
        sRenderer.sprite = slimeSprite[0];

    }

     public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Roca"){
            rb2d.velocity = Vector2.zero;
        }

        else if(other.tag == "Puerta"){
            noControls = true;
            StartCoroutine(LevelSucces());

        }
        else if(other.tag == "Enemy"){
            distance = other.transform.position - transform.position;
            angle = Mathf.Atan2(distance.y, distance.x) * Mathf.Rad2Deg;
            Debug.Log(angle);
            EmpujeDelEnemy();

        }
        else if(other.tag == "Pinchos"){
            StartCoroutine(MuertePlayer());
        }
    }
    IEnumerator LevelSucces(){
            anim.SetTrigger("LevelPassed");
            yield return new WaitForSeconds(2f);
            if(SceneManager.GetActiveScene().name == "GamePlayScreen"){
                SceneManager.LoadScene("Level2");
            }
            else if(SceneManager.GetActiveScene().name == "Level2"){
                SceneManager.LoadScene("Level3");
            }
            else if(SceneManager.GetActiveScene().name == "Level3"){
                SceneManager.LoadScene("Level4");
            }
            else if(SceneManager.GetActiveScene().name == "Level4"){
                SceneManager.LoadScene("Coliseo");
            }
            else if(SceneManager.GetActiveScene().name == "Coliseo"){
                SceneManager.LoadScene("FinalScreen");
            }
            
        }

       
   
       private void EmpujeDelEnemy(){
            
            if(angle == 0){
                rb2d.AddForce(Vector2.left * fuerzaEmpuje);
                StartCoroutine(Esperate0());
            }
            else if(angle > 0 && angle < 90){
                rb2d.AddForce(new Vector2( -1f, -1f) * fuerzaEmpuje);
                StartCoroutine(Esperate1());

            }
            else if(angle == 90){
                rb2d.AddForce(Vector2.down * fuerzaEmpuje);
                StartCoroutine(Esperate2());

            }
            else if(angle > 90 && angle < 180){
                rb2d.AddForce(new Vector2(1, -1f) * fuerzaEmpuje);
                StartCoroutine(Esperate3());

            }
            else if(angle == 180){
                rb2d.AddForce(Vector2.right * fuerzaEmpuje);
                StartCoroutine(Esperate4());

            }
            else if(angle > 180 && angle < 270){
                rb2d.AddForce(new Vector2( 1f, 1f) * fuerzaEmpuje);
                StartCoroutine(Esperate5());

            }
            else if(angle == 270){
                rb2d.AddForce(Vector2.up * fuerzaEmpuje);
                StartCoroutine(Esperate6());

            }
            else if(angle > 270 && angle < 360){
                rb2d.AddForce(new Vector2( 1f, -1f) * fuerzaEmpuje);
                StartCoroutine(Esperate7());

        }
    }

    private void Die(){
        noControls = true;
        sRenderer.sprite = null;
        Debug.Log("muerto");

        Instantiate(FXDead,transform.position,Quaternion.identity);
            
        if(SceneManager.GetActiveScene().name == "GamePlayScreen"){
            if(currentMuerto >= tiempoMuerto){
                SceneManager.LoadScene("GamePlayScreen");
            }
        }
        else if(SceneManager.GetActiveScene().name == "Level2"){
            if(currentMuerto >= tiempoMuerto){
                SceneManager.LoadScene("Level2");
            }
        }
        else if(SceneManager.GetActiveScene().name == "Level3"){
            if(currentMuerto >= tiempoMuerto){
                SceneManager.LoadScene("Level3");
            }
        }
        else if(SceneManager.GetActiveScene().name == "Level4"){
            if(currentMuerto >= tiempoMuerto){
                SceneManager.LoadScene("Level4");
            }
        }
        else if(SceneManager.GetActiveScene().name == "Coliseo"){
            if(currentMuerto >= tiempoMuerto){
                SceneManager.LoadScene("Coliseo");
            }
        }
    }
        IEnumerator Esperate0(){
            yield return new WaitForSeconds(3f);
            Debug.Log("Si");
            rb2d.AddForce(Vector2.left * -fuerzaEmpuje);
        }

        IEnumerator Esperate1(){
            yield return new WaitForSeconds(tiempoDeslizandose);
            Debug.Log("Si");
            rb2d.AddForce(new Vector2( -1f, -1f) * -fuerzaEmpuje);
        }
        IEnumerator Esperate2(){
            yield return new WaitForSeconds(tiempoDeslizandose);
            Debug.Log("Si");
            rb2d.AddForce(Vector2.down * -fuerzaEmpuje);
        }

        IEnumerator Esperate3(){
            yield return new WaitForSeconds(tiempoDeslizandose);
            Debug.Log("Si");
            rb2d.AddForce(new Vector2(1, -1f) * -fuerzaEmpuje);
        }

        IEnumerator Esperate4(){
            yield return new WaitForSeconds(tiempoDeslizandose);
            Debug.Log("Si");
            rb2d.AddForce(Vector2.right * -fuerzaEmpuje);
        }

        IEnumerator Esperate5(){
            yield return new WaitForSeconds(tiempoDeslizandose);
            Debug.Log("Si");
            rb2d.AddForce(new Vector2( 1f, 1f) * -fuerzaEmpuje);
        }

        IEnumerator Esperate6(){
            yield return new WaitForSeconds(tiempoDeslizandose);
            Debug.Log("Si");
            rb2d.AddForce(Vector2.up * -fuerzaEmpuje);
        }

        IEnumerator Esperate7(){
            yield return new WaitForSeconds(tiempoDeslizandose);
            Debug.Log("Si");
            rb2d.AddForce(new Vector2( 1f, -1f) * -fuerzaEmpuje);

        }

        IEnumerator MuertePlayer(){
            Instantiate(FXDead,transform.position,Quaternion.identity);
            Muerte.Play();
            sRenderer.enabled = false;
            noControls = true;
            if(SceneManager.GetActiveScene().name == "Level2" ){
                yield return new WaitForSeconds(2f);
                SceneManager.LoadScene("Level2");
            }
            else if(SceneManager.GetActiveScene().name == "Level3"){
                yield return new WaitForSeconds(2f);
                SceneManager.LoadScene("Level3");
            }
            else if(SceneManager.GetActiveScene().name == "Level4"){
                yield return new WaitForSeconds(2f);
                SceneManager.LoadScene("Level4");
            }
            else if(SceneManager.GetActiveScene().name == "Coliseo"){
                yield return new WaitForSeconds(2f);
                SceneManager.LoadScene("Coliseo");
            }
        }








}
            
            
            
             

