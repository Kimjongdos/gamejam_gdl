﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyController : MonoBehaviour
{
    public int Max;
    public int Min;
    private Rigidbody2D rb2d;
    private Transform target;

    public float maxSpeed;
    public float minSpeed;
    private float speed;

    private PlayerMovement player;

    private Vector3 damage;
    public float timeToDamage;
    private float currentTime;

    public bool attack = false;

    private EnemyManager MangerEnemy;

    public GameObject ParticulaExplosion;
    public GameObject healthPack;
    
    private int Probabilidad;


    void Awake(){
        rb2d = GetComponent<Rigidbody2D>();
        target = GameObject.Find("Player").transform;
        damage = target.localScale/5;
        player = GameObject.Find("Player").GetComponent<PlayerMovement>();
        speed = Random.Range(minSpeed, maxSpeed);
        Debug.Log(speed);
        if(SceneManager.GetActiveScene().name == "Coliseo"){
            attack = true;
            MangerEnemy = GameObject.Find("EnemyManager").GetComponent<EnemyManager>();
        }

         Probabilidad = Random.Range(Min,Max);
        
    }

    void Update(){
        if(attack){
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }
        currentTime += Time.deltaTime;
    }

    void OnTriggerEnter2D(Collider2D other){
        if(other.tag == "Bala"){
            MangerEnemy.DeadsRound1++;
                if(Probabilidad>30 && 50>Probabilidad){ //PACK DE VIDA 
                    Instantiate(healthPack, transform.position, Quaternion.identity);
                }
            
            Instantiate(ParticulaExplosion,transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
        else if (other.tag == "Player"){
            if(currentTime >= timeToDamage){
                other.transform.localScale -= damage;
                currentTime = 0;
            }
        }
       
    }
}
