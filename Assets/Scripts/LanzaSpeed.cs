﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanzaSpeed : MonoBehaviour
{
    public float speed;

    public Rigidbody2D rb2d;

    public GameObject FXLanza;

    // Update is called once per frame
    void FixedUpdate()
    {
      rb2d.velocity = (transform.up * speed * Time.deltaTime);  
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "MuerteLanza"){
            Instantiate(FXLanza,transform.position,Quaternion.identity);
            Destroy(gameObject);
        }
        
    }
}
