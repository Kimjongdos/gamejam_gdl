﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRadius : MonoBehaviour
{
   public EnemyLevels enemyLvl;


    void OnTriggerEnter2D(Collider2D other){
        if(other.tag == "Player"){
            enemyLvl.attack = true;
            Destroy(gameObject,1f);
        }
    }
}
