﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EnemyManager : MonoBehaviour
{
    public GameObject FX;
    public Text Rondass;
    public Transform [] Spawns;
    private int Rand;

    public GameObject Enemy;
    
    private int currentEnemys = -1;

    public float TimeToSpawn;

    private float counter;

    public int Round = 1;

    //Rondas

    public int EnemysInRound1; //Maximo de enemigos por ronda
    public int EnemysInRound2;
    public int EnemysInRound3;

    public float TimepoEntreRondas;

    public bool Round1 = false;
    public bool Round2 = false;
    public bool Round3 = false;

    public int DeadsRound1;
    
    void Start()
    {
        counter = 0;
        currentEnemys = 0;
        
    }

    // Update is called once per frame
    void Update()
    {

        Rondass.text = "ROUND: " + Round.ToString();

        Rand = Random.Range(0,Spawns.Length);

        counter += Time.deltaTime;

        StartCoroutine(Rondas());

        
       
    }   

   

  


    IEnumerator Rondas(){

        switch(Round){

            case 1: //Ronda 1
                if(counter >= TimeToSpawn && currentEnemys < EnemysInRound1 && DeadsRound1 < EnemysInRound1){
                Instancia();
                }

                if(DeadsRound1 >= EnemysInRound1 ){
                        Round1 = true;
                        Debug.Log("Round 1 END");
                        currentEnemys = 0;
                        yield return new WaitForSeconds(TimepoEntreRondas);
                        DeadsRound1 = 0;
                        counter = 0;
                        Round = 2;
                    }
            
            break;

            case 2: //Ronda 2
                
                 
                if(counter >= TimeToSpawn && currentEnemys < EnemysInRound2 && DeadsRound1 < EnemysInRound2){
                Instancia(); 
                }
                if(DeadsRound1 >= EnemysInRound2 ){
                        Round2 = true;
                        Debug.Log("Round 2 END");
                        currentEnemys = 0;
                        yield return new WaitForSeconds(TimepoEntreRondas);
                        
                        counter = 0;
                        DeadsRound1 = 0;
                        Round = 3;
                    }
            

            break;

            case 3: //Ronda 3
            
            if(counter >= TimeToSpawn && currentEnemys < EnemysInRound3 && DeadsRound1 < EnemysInRound3){
                Instancia();

                    
                }
                if(DeadsRound1 >= EnemysInRound3 ){
                      StartCoroutine(Ronda3());
                        
                    }



            break;
            
        }
    }
    public void Instancia(){
        Instantiate(Enemy,Spawns[Rand].position,Quaternion.identity);

        Instantiate(FX,Spawns[Rand].position,Quaternion.identity);

        currentEnemys++;
        counter = 0;
    }
    IEnumerator Ronda3(){
        Round3 = true;
       // Time.timeScale = 0;
        yield return new WaitForSeconds(1f);

        SceneManager.LoadScene("FinalScreen");
       // Time.timeScale = 1f;               
        Debug.Log("Final Round");
        currentEnemys = 0;
        

    }
}
