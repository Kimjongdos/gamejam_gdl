﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShakeFinish : MonoBehaviour
{
   public bool EnterFinish;
   public Animator anim;

     private void OnTriggerEnterStart2D(Collider2D other)
    {
        if(other.tag == "Player"){

            EnterFinish = false;  
            anim.SetBool("Shake",EnterFinish);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.tag == "Player"){

            EnterFinish = true;
            anim.SetBool("Shake",EnterFinish);
        }
    }
}

