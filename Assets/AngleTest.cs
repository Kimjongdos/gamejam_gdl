﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngleTest : MonoBehaviour
{
    private float angle;

    public Transform player;
    public Transform enemy;


    void Awake(){
     
        Vector3 Distancia = enemy.position - player.position;

        float Rotacion = Mathf.Atan2( Distancia.y, Distancia.x) * Mathf.Rad2Deg;
        Debug.Log(Rotacion);
    }

}
